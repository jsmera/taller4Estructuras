#include<iostream>
#include<queue>
#include<vector>
#include<algorithm>
using namespace std;

typedef priority_queue<int, vector<int>, greater<int> >ColaPrioridad;

int main(){
	int n;
	cin>>n;
	while ( n ){
		int value,i=0;
		ColaPrioridad c;
	
	while( i < n ){
		cin>>value;
		c.push(value);
		i++;
	}
	int suma=0,costo=0;
	while( c.size() > 1){
		suma=c.top();
		c.pop();
		suma+=c.top();
		costo+=suma;
		c.push(suma);
		c.pop(); 
	}
	cout<<costo<<endl;
	cin>>n;
	}
	return 0;
}

