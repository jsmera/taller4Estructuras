#include<iostream>
#include<queue>
#include<string>
#include<vector>
using namespace std;

int team[1000001];
queue<int> teamCola[1001];
int main(){
  //t number test cases
  int cases=1;
  cin>>cases;
  int scena=0;
  while( cases ){
    
    //z equipo
    for(int z=0; z<cases; z++){
      int cantNumbers,number;
      int i=0;
      while(!teamCola[z].empty())
        teamCola[z].pop();
      cin>>cantNumbers;
      while(i < cantNumbers){
        cin>>number;
        team[number]=z; 
        i++;
      }
    }
	scena++;
    cout << "Scenario #" << scena <<endl;
    queue<int> simulacion;
    string commands;
    commands="A";
    while(commands[0]!='S'){
      cin>>commands;
      if(commands[0]=='E'){
        int value,equipo;
        cin>>value;
        equipo=team[value];
        if(teamCola[equipo].empty()){
          simulacion.push(equipo);
        }
        teamCola[equipo].push(value);
      }
      if(commands[0]=='D'){
        //para que me devuelva el que sale de primero
        int who,num;
        who=simulacion.front();
        num=teamCola[who].front();
        teamCola[who].pop();
        cout<<num<<endl;
        if(teamCola[who].empty()){
          simulacion.pop();
        }
      }
    }
    cout<<endl;
    cin>>cases;
    }
  return 0;
}
